export default {
  db: {
    url: "mongodb://localhost:27017/vending_machine"
  },
  models: {
  },
  security: {
  },
  session: {
    cookie: {
      secure: true,
      maxAge: false //24 * 60 * 60 * 1000 // 24 hours
    }
  },
  sockets: {
  },
  log: {
    level: "debug"
  },
  http: {
    cache: 365.25 * 24 * 60 * 60 * 1000 // One year
  },
  port: 7000,
};

