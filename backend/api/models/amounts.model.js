
import mongoose from 'mongoose';
import bcrypt from "bcryptjs";
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;

let productsSchema = new Schema({
    amount: {
        type: String,
        default: null
    },
    
}, { timestamps: true });


export default mongoose.model('Amounts', productsSchema);