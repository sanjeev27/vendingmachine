
import mongoose from 'mongoose';
import bcrypt from "bcryptjs";
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;

let productsSchema = new Schema({
    name: {
        type: String,
        default: null
    },
    amount: {
        type: String,
        default: null
    },
    quantity: {
        type: String,
        default: 0
    },
    inStock: {
    	type: Boolean,
    	default: true
    },
    productImage: {
    	type: String,
    	default: null
    },
    serialNo: {
    	type: String,
    	default: null
    },
    description: {
    	type: String,
    	default: null
    }
    
}, { timestamps: true });


export default mongoose.model('Products', productsSchema);