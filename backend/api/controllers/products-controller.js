import bcrypt from "bcryptjs";
import { err500S, allMandatoryFieldsRequiredS } from "../utilities/error-functions";
import { Products } from '../models';
import path from "path";



export const getProducts = async function (req, res) {
	let products = await Products.find({});
	return res.status(200).json({
        message: "Products fetched successfully.",
        data: products,
        productImageUrl: "assets/products/",
        status: true
    });

};

export const buyProduct = async function (req, res) {
	const {productId} = req.body;
	let product = await Products.findOne({_id: productId});
	let productQuantity = parseInt(product.quantity) - 1;
	let inStock = productQuantity == '0' ? false : true

	await Products.findOneAndUpdate({ '_id': productId }, {
        $set: {
            "quantity": productQuantity,
            "inStock": inStock
        }
    }, { useFindAndModify: false }, async function (err, data) {
        if (err) {
            return res.status(500).json({
                message: "Something went wrong. Please collect your cash and try again.",
                status: true
            });
        }
        return res.status(200).json({
            message: "Product purchased successfully.",
            status: false
        });
        
    });


};