import jwt from "jsonwebtoken";
import bcrypt from "bcryptjs";
import { err500S, allMandatoryFieldsRequiredS } from "../utilities/error-functions";
import { User } from '../models';
import { sign } from "../utilities/jwt";
import path from "path";


/*export const deleteUser = async function (req, res) {
	const {userId} = req.body;
	if (!userId) {
    	return allMandatoryFieldsRequiredS(res);
	}

	await User.deleteOne({ _id: userId }, function(err) {
	    if (err) {
         	return err500S(res);
	    }
	    return res.status(200).json({
	        message: "User deleted successfully.",
	        status: true
	    });
	});

};*/