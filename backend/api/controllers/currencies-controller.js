import bcrypt from "bcryptjs";
import { err500S, allMandatoryFieldsRequiredS } from "../utilities/error-functions";
import { Amounts } from '../models';
import path from "path";



export const getCurrencies = async function (req, res) {
	const {userId} = req.body;
	let currencies = await Amounts.find({});
	return res.status(200).json({
	        message: "Currencies fetched successfully.",
	        data: currencies,
	        status: true
	    });

};