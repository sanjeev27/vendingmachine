import express from "express";
import { getCurrencies } from "../controllers/currencies-controller";


const router = express.Router();

router.get("/currencies", getCurrencies);


export default router;





