import express from "express";
import { getProducts, buyProduct } from "../controllers/products-controller";


const router = express.Router();

router.get("/get-products", getProducts);
router.post("/buy-product", buyProduct);


export default router;





