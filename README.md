# vendingmachine


**For backend :**


1. Clone the repository.

2. Open app.js from root directory as app.js being the starting point of the project.

3. Environments are set in package.json using nodemon.
    a. cd into backend folder and run "npm install to install the dependencies.
    b. For windows the environment is set using "SET NODE_ENV=local && nodemon app.js  --exec babel-node", into scripts of package.json. Just cd   into the backend directory and run run local.

    Note: If you are running this on MAC please change the  "SET NODE_ENV=local && nodemon app.js  --exec babel-node" to "NODE_ENV=local && nodemon app.js  --exec babel-node" -> Just remove SET from the line and run npm run local.

4. Make sure to have nodemon installed globally on your machine

Currenlty the setup has local environment config in /config/env/local.js

5. Database name is mentioned in backend/config/env/local.js 
    Please import the database file as it contains some master tables . The current name used is vending_machine you can change it into the local.js



**For Frontend:**

1. cd into frontend folder 
2. Run npm install 
3. ng serve to start the application 


**Database**

Please import the db as it contains master files . Db file is in root **database_files** folder



Note: default ports used are 7000 for backend and 4200 for frontend . Please try to use the same as cors proxy is handled for this ports . Incase top change it please change backend port in /config/env/local.js 

