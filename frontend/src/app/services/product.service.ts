import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ProductDataService {
    selectedProduct:any;
    validCurrencies:any;
    constructor() { }

    setSelectedProduct(product) {
      this.selectedProduct = product;
    }

    setCurrencies(currencies) {
    	this.validCurrencies = currencies;
    }

    getCurrencies() {
    	return this.validCurrencies;
    }

    getSelectedProduct() {
    	return this.selectedProduct;
    }
}
