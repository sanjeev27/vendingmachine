import { Injectable } from "@angular/core";
import { HttpClient, HttpHeaders } from "@angular/common/http";
import { environment } from '../../environments/environment';
import { timeout } from 'rxjs/operators';


@Injectable({
  providedIn: "root",
})
export class ApiService {
  public baseUrl = environment.apiUrl;

  private commonHeader = new HttpHeaders({
    "Content-Type": "application/json",
    Accept: "application/json",
  });

  constructor(
    public http: HttpClient,
  ) {}

  getData(url: string, isToken: boolean = false) {
    let newHeader: any;
    return new Promise((resolve, reject) => {
        if (isToken) {
          let authToken = localStorage.getItem("mean-stack-token");
          newHeader = this.commonHeader.append(
            "Authorization",
            "Bearer " + authToken
          );
        } else {
          newHeader = this.commonHeader;
        }
        this.http.get(this.baseUrl + url, { headers: newHeader })
        .pipe(timeout(10000))
        .subscribe(
          (res) => {
            resolve(res);
          },
          (err) => {
            if (err.error.status == -1) {
              reject(err.error);
            } else {
              reject(err.error);
            }
          }
        );
    });
  }

  postData(data: any, url: string, isToken = false, isFileUpload = false) {
    let newHeader: any;
    return new Promise((resolve, reject) => {
        if (isToken) {
          let authToken = localStorage.getItem("mean-stack-token");
          newHeader = this.commonHeader.append(
            "Authorization",
            "Bearer " + authToken
          );
        } else {
          newHeader = this.commonHeader;
        }
        if(isFileUpload) {
          newHeader = new HttpHeaders({
            "enctype": "multipart/form-data",
            Accept: "application/json",
          });
        }
        
        this.http
          .post(this.baseUrl + url, data, { headers: newHeader })
          .pipe(timeout(10000))
          .subscribe(
            (res) => {
              resolve(res);
            },
            (err) => {
              if (err.error.status == -1) {
                // this.authService.logout();
                reject(err);
              } else {
                reject(err);
              }
            }
          );
    });
  }
}
