import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
{
    path: '',
    redirectTo: 'products',
    pathMatch: 'full'
},
{ path: 'products', loadChildren: () => import('./products/products.module').then(m => m.ProductsModule) },
{ path: 'enter-amount', loadChildren: () => import('./enter-amount/enter-amount.module').then(m => m.EnterAmountModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
