import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services';
import { ProductDataService } from 'src/app/services';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css']
})
export class ProductsComponent implements OnInit {
	public baseUrl = environment.apiUrl;
	products:any;
    currencies:any;
	productImagePath:any;
	selectedProduct:any;
    constructor(private apiService:ApiService, private productService: ProductDataService, private router: Router) { }

    ngOnInit() {
        this.getCurrencies();
	  	this.apiService.getData('product/get-products', false).then((result) => {
	    	let responseData:any = result;
	    	this.products = responseData.data;
	    	this.productImagePath = responseData.productImageUrl;
	  	},
	        (err) => {
		        console.log(err);	  
	        if(err && err.error && err.error.message) {
	          alert(err.error.message);
	        }       
	  	});
    }

    productSelected(product) {
    	this.selectedProduct = '';
    	if(!product.inStock) {
    		return false;
    	}
    	this.selectedProduct = product;
    	console.log(this.selectedProduct);
    	this.productService.setSelectedProduct(this.selectedProduct);
    	this.router.navigate(['/enter-amount']);
    }

    getCurrencies() {
        this.apiService.getData('money/currencies', false).then((result) => {
            let responseData:any = result;
            this.currencies = responseData.data;
            this.productService.setCurrencies(this.currencies)
        },
            (err) => {
                console.log(err);     
            if(err && err.error && err.error.message) {
              alert(err.error.message);
            }       
        });
    }

}
