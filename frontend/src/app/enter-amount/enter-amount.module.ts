import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EnterAmountRoutingModule } from './enter-amount-routing.module';
import { EnterAmountComponent } from './enter-amount.component';
import {FormsModule} from "@angular/forms";


@NgModule({
  declarations: [EnterAmountComponent],
  imports: [
    CommonModule,
    FormsModule,
    EnterAmountRoutingModule
  ]
})
export class EnterAmountModule { }
