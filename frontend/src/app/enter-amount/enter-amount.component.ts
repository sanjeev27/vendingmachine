import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services';
import { ProductDataService } from 'src/app/services';
@Component({
  selector: 'app-enter-amount',
  templateUrl: './enter-amount.component.html',
  styleUrls: ['./enter-amount.component.css']
})
export class EnterAmountComponent implements OnInit {
	selectedProduct:any;
	validCurrencies:any;
	validCurrenciesArray:any = [];
	enteredAmount:any;
  	constructor(private router: Router, private productService: ProductDataService, private apiService:ApiService) { 
  		this.selectedProduct = this.productService.getSelectedProduct();
  		this.validCurrencies = this.productService.getCurrencies();
  		console.log(this.validCurrencies);
  		if(!this.selectedProduct ) {
  			this.backToProducts();
  		}
  		for(let i = 0; i <this.validCurrencies.length; i++) {
  			this.validCurrenciesArray.push(parseInt(this.validCurrencies[i].amount))
  		}
  	}

 	ngOnInit() {
	}

  	backToProducts() {
	  	this.router.navigate(['products']);
  	}

  	buyProduct() {
  		if(isNaN(this.enteredAmount)) {
  			alert('Enter Valid Amount. Accepted currencies are '+this.validCurrenciesArray.join(", "));
  		}
  		let enteredAmount = parseInt(this.enteredAmount);
  		if(this.validCurrenciesArray.indexOf(enteredAmount) > -1) {
  			if(enteredAmount < parseInt(this.selectedProduct.amount)) {
  				let diffAmount = parseInt(this.selectedProduct.amount) - enteredAmount;
  				alert("Please enter Rs " + diffAmount + " more.")
  				return;
  			}
  			this.apiService.postData({ productId: this.selectedProduct._id}, 'product/buy-product' , false).then((result) => {
		    	let responseData:any = result;
		    	this.products = responseData.data;
		    	if(enteredAmount > parseInt(this.selectedProduct.amount)) {
	  				let diffAmount = enteredAmount - parseInt(this.selectedProduct.amount);
	  				alert("Product Purchased Successfully. Please collect your change back. Thank You!")
	  			} else {
	  				alert('Product Purchased Successfully. Thank You !')
	  			}
	  			this.backToProducts();
		  	},
		        (err) => { 
		        alert('Something went wrong . Please collect your cash and try again.') ;
		        this.enteredAmount = '';   
		  	});
  		} else {
  			alert('Accepted currencies are '+this.validCurrenciesArray.join(", "))
  		}
  	}

}
