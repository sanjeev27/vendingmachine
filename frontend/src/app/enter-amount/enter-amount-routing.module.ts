import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EnterAmountComponent } from './enter-amount.component';

const routes: Routes = [{ path: '', component: EnterAmountComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EnterAmountRoutingModule { }
